class ArbolCY {

    constructor() {
        this.nodoPadre = "";
        this.nodos = [];
        //this.nivel = 3;
        //this.busquedaElemento = null;
        //this.buscarNodos = [];
        //this.caminoNodo = '';
        //this.sumarCamino= 0;
    }

    agregarNodoPadre(nodoPadre,posicion,nombre,valor,nivel){
        var nodo = new NodoCY(null,null,nombre,valor,nivel);
        this.nodos.push(nodo);
        return nodo;
    }

    agregarNodo(nodoPadre,posicion,nombre,valor,nivel){
        var nodo = new NodoCY(nodoPadre,posicion,nombre,valor,nivel);
        this.nodos.push(nodo);
        return nodo;
    }

    agregarNodoNuevo(valor, nombre){
        let nodo = new NodoCY(null, null, nombre, valor, null);
        if(this.nodos.length == 1){
            nodo.padre = this.nodoPadre;
            nodo.nivel = this.nodoPadre.nivel +1;
            if(nodo.valor < this.nodoPadre.valor){
                //--Izquierda
                nodo.posicion = 'HI';
            }else{
                nodo.posicion = 'HD';
            }this.nodos.push(nodo);
        }
        
    }

    buscarNodosPadre(padre){
        let nodosEncontrados = [];
        for(let x =0; x<this.nodos; x++){
            if(this.nodos[x].padre == padre)
            nodosEncontrados.push(this.nodos[x]);
        }
    }

    verificarNivelHijos(nodo){

        if(nodo.nivel == this.nivel)
            this.buscarNodos.push(nodo.nombre);

        if(nodo.hasOwnProperty('hI'))
            this.verificarNivelHijos(nodo.hI);

        if(nodo.hasOwnProperty('hD'))
            this.verificarNivelHijos(nodo.hD);

        return this.buscarNodos;

    }

    buscarValor(buscarElemento,nodo){

        if(nodo.valor == buscarElemento && this.busquedaElemento == null)
            this.busquedaElemento = nodo;

        if(nodo.hasOwnProperty('hI'))
            this.buscarValor(buscarElemento,nodo.hI);

        if(nodo.hasOwnProperty('hD'))
            this.buscarValor(buscarElemento,nodo.hD);

        
        return this.busquedaElemento;
        

        
    }

    buscarCaminoNodo(nodo){
        if(nodo.padre != null){
            this.caminoNodo = this.caminoNodo+' '+nodo.padre.nombre;
            this.buscarCaminoNodo(nodo.padre);
        }
        
        return this.busquedaElemento.nombre+' '+this.caminoNodo;
    }

    sumarCaminoNodo(nodo){
        if(nodo.padre != null){
            this.sumarCamino = this.sumarCamino+nodo.padre.valor;
            this.sumarCaminoNodo(nodo.padre);
        }
        
        return this.busquedaElemento.valor+this.sumarCamino;
    }

    



}