class NodoCY{
    constructor(nodoPadre = null, posicion = null,nombre, valor, nivel=null) {
        this.nombre = nombre;
        this.padre = nodoPadre;
        this.posicion = posicion;
        this.valor = valor;
        this.nivel = nivel;
    }
}